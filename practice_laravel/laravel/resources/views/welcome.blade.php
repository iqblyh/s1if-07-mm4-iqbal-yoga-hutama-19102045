<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #171717;
                color: #fff;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
                
            }
            
            .lines {
                position: absolute;
                top: 0;
                left: 0;
                right: 0;
                height: 100%;
                margin: auto;
                width: 90vw;
            }

            .line {
                position: absolute;
                width: 1px;
                height: 100%;
                top: 0;
                left: 50%;
                background: rgba(255, 255, 255, 0.1);
                overflow: hidden;
            }
            .line::after {
                content: "";
                display: block;
                position: absolute;
                height: 15vh;
                width: 100%;
                top: -50%;
                left: 0;
                background: linear-gradient(to bottom, rgba(255, 255, 255, 0) 0%, #ffffff 75%, #ffffff 100%);
                -webkit-animation: drop 7s 0s infinite;
                        animation: drop 7s 0s infinite;
                -webkit-animation-fill-mode: forwards;
                        animation-fill-mode: forwards;
                -webkit-animation-timing-function: cubic-bezier(0.4, 0.26, 0, 0.97);
                        animation-timing-function: cubic-bezier(0.4, 0.26, 0, 0.97);
            }
            .line:nth-child(1) {
                margin-left: -25%;
            }
            .line:nth-child(1)::after {
                -webkit-animation-delay: 2s;
                        animation-delay: 2s;
            }
            .line:nth-child(3) {
                margin-left: 25%;
            }
            .line:nth-child(3)::after {
                -webkit-animation-delay: 2.5s;
                        animation-delay: 2.5s;
            }

            @-webkit-keyframes drop {
                0% {
                    top: -50%;
                }
                100% {
                    top: 110%;
                }
            }

            @keyframes drop {
                0% {
                    top: -50%;
                }
                100% {
                    top: 110%;
                }
            }
            
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif
            
            

            <div class="content">
                <div class="lines">
                <div class="line"></div>
                <div class="line"></div>
                <div class="line"></div>
            </div>
                <div class="title m-b-md">
                    Iqbal Yoga Hutama
                </div>

                <div class="links">
                    <a href="https://laravel.com/docs">Docs</a>
                    <a href="https://laracasts.com">Laracasts</a>
                    <a href="https://laravel-news.com">News</a>
                    <a href="https://blog.laravel.com">Blog</a>
                    <a href="https://nova.laravel.com">Nova</a>
                    <a href="https://forge.laravel.com">Forge</a>
                    <a href="https://vapor.laravel.com">Vapor</a>
                    <a href="https://github.com/laravel/laravel">GitHub</a>
                </div>
            </div>
        </div>
    </body>
</html>
